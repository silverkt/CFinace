-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2017-07-05 18:54:36
-- 服务器版本： 10.2.6-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cf618`
--
CREATE DATABASE IF NOT EXISTS `cf618` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cf618`;

-- --------------------------------------------------------

--
-- 表的结构 `admin_tb`
--

CREATE TABLE `admin_tb` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `passwd` varchar(32) NOT NULL DEFAULT ' ',
  `name` varchar(20) NOT NULL DEFAULT ' ',
  `mobile` varchar(20) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `admin_tb`
--

INSERT INTO `admin_tb` (`id`, `passwd`, `name`, `mobile`) VALUES
(1, '1234', '小青', '15216817449');

-- --------------------------------------------------------

--
-- 表的结构 `reg_table`
--

CREATE TABLE `reg_table` (
  `id` int(11) NOT NULL,
  `loan_sum` int(11) NOT NULL,
  `property` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mobile` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `reg_table`
--

INSERT INTO `reg_table` (`id`, `loan_sum`, `property`, `mobile`) VALUES
(1, 50, '无房产', '16261616662'),
(2, 400, '经济适用/限价房', '182881888182881'),
(3, 400, '经济适用/限价房', '182881888182881'),
(4, 200, '经济适用/限价房', '152168174492'),
(5, 90, '办公楼', '162661222'),
(6, 90, '商铺', '162661222'),
(7, 6060, '房改/危改房', '166262373838'),
(9, 19, '无房产', '1666262222'),
(10, 1000, '商住两用', '15216817449'),
(11, 98, '商住两用', '162515151511'),
(12, 111, '商业住宅', '2991991'),
(13, 234234, '无房产', '234234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_tb`
--
ALTER TABLE `admin_tb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reg_table`
--
ALTER TABLE `reg_table`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `admin_tb`
--
ALTER TABLE `admin_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `reg_table`
--
ALTER TABLE `reg_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
