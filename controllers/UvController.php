<?php

namespace app\controllers;

use Yii;
use app\models\RegTable;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\AdminTb;

/**
 * UvController implements the CRUD actions for RegTable model.
 */
class UvController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RegTable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RegTable::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RegTable model.
     * @param integer $id
     * @return mixed
     */
    public function actionI($id)
    {
        if (RegTable::findOne($id)) {
            $ref = RegTable::findOne($id)->ref_id;
        } else {
            return $this->redirect(['vcode']);
        }
        
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has('vcode') && md5($ref) ==  $cookies->get('vcode')) {
            return $this->renderPartial('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->redirect(['vcode']);
        }
    }
    
    
     public function actionVcode()
    {
        if (Yii::$app->request->post()) {
            $vcode = Yii::$app->request->post('vcode');
            if (AdminTb::findOne(['vcode' => $vcode])) {
                 $ref = AdminTb::findOne(['vcode' => $vcode])->id;
                 // 从"response"组件中获取cookie 集合(yii\web\CookieCollection)
                $cookies = Yii::$app->response->cookies;
                // 在要发送的响应中添加一个新的cookie
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'vcode',
                    'value' => md5($ref),
                    'expire' => time() + 10 * 365 * 24 * 60 * 60
                ]));
            }        
            Yii::$app->session->setFlash('contactFormSubmitted');             
            return $this->refresh();
        }
        
        return $this->renderPartial('vcode');
    }

    /**
     * Creates a new RegTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RegTable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RegTable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);
        $model->scenario = 'dapply';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['i', 'id' => $model->id]);
        } else {
            return $this->renderPartial('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RegTable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RegTable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RegTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RegTable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
