<?php

namespace app\controllers;

use Yii;
use app\models\RegTable;
use app\models\AdminTb;
 
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



/**
 * RegController implements the CRUD actions for RegTable model.
 */
class RegController extends Controller
{
    //public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RegTable models.
     * @return mixed
     */
    public function actionIndex()
    {
         return $this->renderPartial('error');
    }

    /**
     * Displays a single RegTable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
       return $this->renderPartial('error');
    }

    /**
     * Creates a new RegTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RegTable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RegTable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RegTable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RegTable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RegTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RegTable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function  findAdminId($id) {
        if(AdminTb::findOne($id)!==null) {
            return true;
        } else {
            return false;
        }
    }

    

    public function actionApply()
    {
        
        $model = new RegTable();
        $model->ref_id = Yii::$app->request->get('id');
        $model->scenario = 'register';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            if($model->loan_type == '抵押贷款') {
                $this->redirect(['reg/applyd','id'=>$model->id]);  
            } else {
                $this->redirect(['reg/applyx','id'=>$model->id]);  
            }

        }  
                
                
        if($this->findAdminId($model->ref_id)) {
             return $this->renderPartial('apply', [
            'model' => $model,
        ]);
        } else {
             return $this->renderPartial('error');
        }  
        
    }
    public function actionApplyd($id)
    {
        $model = RegTable::findOne(['id'=>$id]);
        $model->scenario = 'dapply';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {    
            $sms = $model->mobile.$model->name.$model->sex.'，详见cf618.com/index.php?r=uv/i&id='.$model->id;
            $mobile  = AdminTb::findOne(['id' => $model->ref_id ])->mobile;
            RegTable::sendInfo($mobile, $sms);
            return $this->renderPartial('success');
         }  
         return $this->renderPartial('dapply', [
            'model' => $model,
             ]);
    }
    
    
    
    public function actionApplyx($id)
    {
        $model = RegTable::findOne(['id'=>$id]);
        $model->scenario = 'xapply';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {    
            $sms = $model->mobile.$model->name.$model->sex.'，详见cf618.com/index.php?r=uc/i&id='.$model->id;
            $mobile  = AdminTb::findOne(['id' => $model->ref_id ])->mobile;
            RegTable::sendInfo($mobile, $sms);
            return $this->renderPartial('success');
         }  
         return $this->renderPartial('xapply', [
            'model' => $model,
             ]);
    }
    
    
    
        /*
     * 为前段ajax调用发送短信验证码
     *   */
    public function actionSendCode()
    {
        if (Yii::$app->request->post('opr') == 'getcode') {
            RegTable::sendVerCode();
            exit;
        }
        //return $this->render('about');
    }
    
    
     
}
