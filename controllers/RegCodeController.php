<?php

namespace app\controllers;

use Yii;
use app\models\Regcode;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegCodeController implements the CRUD actions for Regcode model.
 */
class RegCodeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Regcode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Regcode::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Regcode model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Regcode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Regcode();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionTest()
    {
        $model = new Regcode();
        $model->code = 'bbbb';
        $model->expired = 0;
        $model->date = time().'a';
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
       
        
               
        if($model->save()) {
             $response->data =$model;
        } else {
            return $this->render('shit', [
                'shit' => 'failure',
            ]);
        }

    }
    
    /**
     * Generated VerCode
     */
        
    public function actionGenerate()
    {

        $model = new Regcode();
        $model->code = rand(0,9).substr(time(),-4);
        $model->expired = 0;
        $model->date = time().'';
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        
       
        
               
        if($model->save()) {             
            // $response->data = $model;     
             $resData = [
                 'code' => $model->id.$model->code
             ];
             $response->data = $resData;
        } else {
              $resData = [
                 'code' => '生成错误'
             ];
             $response->data = $resData;      
        }

    }

    /**
     * Updates an existing Regcode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Regcode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Regcode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regcode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Regcode::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
