<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reg-table-form">

    <?php $form = ActiveForm::begin(); ?>

  
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sex')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'loan_sum')->textInput() ?>

 

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

 

    <?= $form->field($model, 'loan_type')->textInput(['maxlength' => true]) ?>



 

    <?= $form->field($model, 'customer_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gongjijin')->textInput(['maxlength' => true]) ?>

 

    <?= $form->field($model, 'pay_cash')->textInput() ?>

    <?= $form->field($model, 'housing_loan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'housing_loan_per')->textInput() ?>

    <?= $form->field($model, 'housing_loan_age')->textInput() ?>

    <?= $form->field($model, 'enterprise_age')->textInput() ?>

    <?= $form->field($model, 'industry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'enterprise_cash')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : '更新客户信息', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
