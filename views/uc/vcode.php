<?php
use yii\helpers\Html;
 
use yii\bootstrap\ActiveForm;
 
$this->registerCssFile("assets/css/bootstrap.min.css");
 


 
$this->registerJsFile('assets/js/bootstrap.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
 
 
$this->title = "验证识别码";
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      
      
      
      
    <div class="container-fluid">
<div class="reg-table-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?> 
已提交，可再次访问短信里链接
         <?php else: ?>
                <?php $form = ActiveForm::begin(['id' => 'contact-form']);  ?>
           <?= Html::input('text', 'vcode'); ?>
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    <?php ActiveForm::end();   ?>
         <?php endif;?>
            
    </p>

   

</div>
 </div>

 
    
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>