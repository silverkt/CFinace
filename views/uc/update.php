<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
 
$this->registerCssFile("assets/css/bootstrap.min.css");
 


 
$this->registerJsFile('assets/js/bootstrap.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
 
 
$this->title = $model->name.$model->sex;

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      
      
      
      
    <div class="container-fluid">
<div class="reg-table-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>

 
    
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>