<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reg Tables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-table-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reg Table', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'loan_sum',
            'property',
            'mobile',
            'city',
            // 'loan_type',
            // 'ref_id',
            // 'sex',
            // 'name',
            // 'backup_property',
            // 'customer_type',
            // 'gongjijin',
            // 'tax_list',
            // 'pay_cash',
            // 'housing_loan',
            // 'housing_loan_per',
            // 'housing_loan_age',
            // 'enterprise_age',
            // 'industry',
            // 'enterprise_cash',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
