<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdminTb */

$this->title = 'Update Admin Tb: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Admin Tbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admin-tb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
