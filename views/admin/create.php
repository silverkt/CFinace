<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdminTb */

$this->title = 'Create Admin Tb';
$this->params['breadcrumbs'][] = ['label' => 'Admin Tbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-tb-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
