<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Regcode */

$this->title = 'Create Regcode';
$this->params['breadcrumbs'][] = ['label' => 'Regcodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regcode-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
