<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RegTable */

$this->title = 'Create Reg Table';
$this->params['breadcrumbs'][] = ['label' => 'Reg Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
