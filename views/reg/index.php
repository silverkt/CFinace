<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reg Tables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reg-table-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Reg Table', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'loan_sum',
            'property',
            'mobile',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
