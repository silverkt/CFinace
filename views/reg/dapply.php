<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Request;
use yii\web\View;
$this->registerCssFile("assets/css/bootstrap.min.css");
 


 
$this->registerJsFile('assets/js/bootstrap.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
 
 


?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      
      
      
      
    <div class="container-fluid">
        <br>
        
        <?php $form = ActiveForm::begin(); ?>
         <?= $form->field($model, 'sex', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['先生' =>'先生','女士'=>'女士'],[
   'prompt' => ['text' => '称呼', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>
        <?= $form->field($model, 'name', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的姓名']) ?>
        <?= $form->field($model, 'property', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的房产总价(万元)'])->label('')  ?>
       
             <?= $form->field($model, 'backup_property', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['有备用房' =>'有备用房','无备用房'=>'无备用房'],[
   'prompt' => ['text' => '您是否有备用房', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>   
        
        
        
    
        <div class="form-group">
            <?= Html::submitButton('立刻申请', ['class' => 'btn btn-danger', 'style' => 'width:100%;']) ?>
        </div>
    <?php ActiveForm::end(); ?>
 
    
   
        


 

    </div>

 
    
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>