<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Request;
use yii\web\View;
$this->registerCssFile("assets/css/bootstrap.min.css");
$this->registerCssFile("https://cdn.bootcss.com/Swiper/3.4.2/css/swiper.min.css");



  $this->registerJsFile('assets/js/jquery.min.js');
        $this->registerJsFile('assets/js/bootstrap.min.js');
$this->registerJsFile('https://cdn.bootcss.com/Swiper/3.4.2/js/swiper.jquery.min.js');
$this->registerJs('  
    var mySwiper = new Swiper (\'.swiper-container\', {
    direction: \'horizontal\',
    loop: true,
    autoplay : 3000,
    
    // 如果需要分页器
    pagination: \'.swiper-pagination\',
    
    // 如果需要前进后退按钮
//    nextButton: \'.swiper-button-next\',
//    prevButton: \'.swiper-button-prev\',
    
    // 如果需要滚动条
//    scrollbar: \'.swiper-scrollbar\',
  })      ',   View::POS_END, 'my-swiper');
 


?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      <div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide"><img src="assets/timg.jpg" style="width:100%;"></div>
        <div class="swiper-slide"><img src="assets/timg1.png" style="width:100%;"></div>
        <div class="swiper-slide"><img src="assets/timg2.png" style="width:100%;"></div>
    </div>
    <!-- 如果需要分页器 -->
    <div class="swiper-pagination"></div>
    
<!--     如果需要导航按钮 
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>-->
    
    <!-- 如果需要滚动条 -->
<!--    <div class="swiper-scrollbar"></div>-->
</div>
      
      
      
    <div class="container-fluid">
        
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'loan_sum', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您想要贷款的额度']) ?>
        <?= $form->field($model, 'mobile', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的手机号'])->label('')  ?>
        <?= $form->field($model, 'vercode', [
             'template' => '<div class="input-group"> {input}<span class="input-group-btn"><button id=getcode class="btn btn-success" disabled=true type="button">获取验证码</button></span> </div> {hint} {error}'
         ])->input('text', ['placeholder' => '在此填入您手机收到的验证码']) ?> 
        <?= $form->field($model, 'passwd', [
             'template' => ' {input} {hint} {error}'
         ])->input('password', ['placeholder' => '请输入密码']) ?>
        <?= $form->field($model, 'confirmpass', [
             'template' => ' {input} {hint} {error}'
         ])->input('password', ['placeholder' => '请再次输入密码以确认']) ?>
        
        
    
        <div class="form-group">
            <?= Html::submitButton('注册', ['class' => 'btn btn-danger', 'style' => 'width:100%;']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    
    
    
        <form method="post" action="./index.php?r=reg/apply"   >
            
            <?= $form->field($model, 'vercode', [
             'template' => '<div class="input-group"> {input}<span class="input-group-btn"><button id=getcode class="btn btn-success" disabled=true type="button">获取验证码</button></span> </div> {hint} {error}'
         ])->input('text', ['placeholder' => '在此填入您手机收到的验证码']) ?> 
            
        <div class="row">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10"><br>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="100" aria-describedby="basic-addon2" name="loan">
                <span class="input-group-addon" id="basic-addon2">万元</span>
              </div>
        </div>
        <div class="col-xs-1"> </div>
      </div>
      <div class="row">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10"><br>
            <div class="input-group">
                <select class="form-control" name="property">
                            <option>无房产</option>
                            <option>商业住宅</option>
                            <option>经济适用/限价房</option>
                            <option>商铺</option>
                            <option>办公楼</option>
                            <option>商住两用</option>
                            <option>厂房</option>
                            <option>房改/危改房</option>
                            <option>小产权房</option>
                            <option>宅基地/自建房</option>
                            <option>军产房</option>
                          </select>
                <span class="input-group-addon" id="basic-addon2">房产</span>
              </div>
        </div>
        <div class="col-xs-1"> </div>
      </div>
             <div class="row">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10">
            <br>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="100" aria-describedby="basic-addon2" name="mobile">
                <span class="input-group-addon" id="basic-addon2">手机号</span>
              </div>
        </div>
        <div class="col-xs-1"> </div>
      </div> 
            
             <div class="row">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10">
            <br>
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">

            <button type=submit class="btn btn-danger" style="width:100%;">提交</button>    
        </div>
        <div class="col-xs-1"> </div>
      </div> 
                 
      </form> 
        


 

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 
  
    <!-- Include all compiled plugins (below), or include individual files as needed -->
 
    
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>