<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Request;
use yii\web\View;
$this->registerCssFile("assets/css/bootstrap.min.css");
$this->registerCssFile("https://cdn.bootcss.com/Swiper/3.4.2/css/swiper.min.css");


 
$this->registerJsFile('assets/js/bootstrap.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdn.bootcss.com/Swiper/3.4.2/js/swiper.jquery.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJs('  
    var mySwiper = new Swiper (\'.swiper-container\', {
    direction: \'horizontal\',
    loop: true,
    autoplay : 3000,
    
    // 如果需要分页器
    pagination: \'.swiper-pagination\',
    
    // 如果需要前进后退按钮
//    nextButton: \'.swiper-button-next\',
//    prevButton: \'.swiper-button-prev\',
    
    // 如果需要滚动条
//    scrollbar: \'.swiper-scrollbar\',
  }) ',   View::POS_END, 'my-swiper');
 


?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      <div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide"><img src="assets/timg.jpg" style="width:100%;"></div>
        <div class="swiper-slide"><img src="assets/timg1.png" style="width:100%;"></div>
        <div class="swiper-slide"><img src="assets/timg2.png" style="width:100%;"></div>
    </div>
    <!-- 如果需要分页器 -->
    <div class="swiper-pagination"></div>
    
<!--     如果需要导航按钮 
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>-->
    
    <!-- 如果需要滚动条 -->
<!--    <div class="swiper-scrollbar"></div>-->
</div>
      
      
      
    <div class="container-fluid">
        <br>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'loan_sum', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您想要贷款的额度（万元）']) ?>
        <?= $form->field($model, 'mobile', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的手机号'])->label('')  ?>
        <?= $form->field($model, 'vercode', [
             'template' => '<div class="input-group"> {input}<span class="input-group-btn"><button id=getcode class="btn btn-success" disabled=true type="button">获取验证码</button></span> </div> {hint} {error}'
         ])->input('text', ['placeholder' => '在此填入您收到的验证码']) ?> 
        <?= $form->field($model, 'loan_type', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['抵押贷款' =>'抵押贷款','信用贷款'=>'无抵押贷款'],[
   'prompt' => ['text' => '选择贷款类型', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>
        
        
        
    
        <div class="form-group">
            <?= Html::submitButton('立刻申请', ['class' => 'btn btn-danger', 'style' => 'width:100%;']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    <?php
       $js = '
 
            $("#regtable-mobile").keyup(function(){
            	var pattern = /^(13[0-9]|17[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/;
            	var phoneno = $(this).val();
                if (pattern.test(phoneno)) {         
                    $("#getcode").removeAttr("disabled");
                     $("#getcode").removeClass("btn-default");
                     $("#getcode").addClass("btn-success");
                } else {
                    $("#getcode").prop("disabled","true");
                }	
            });
            $("#getcode").click(function(){
                var phoneno = $("#regtable-mobile").val();
                $("#getcode").prop("disabled","true");
               
                $("#getcode").removeClass("btn-success");
                $("#getcode").addClass("btn-default");
                $.post("index.php?r=reg/send-code",{"opr":"getcode","mobile":phoneno},function(data){
                    data = String(data);      
                    if (data == "codesent") {
                     $("#getcode").html("验证码发出");
                        $("#regtable-vercode").focus();
                    }
                    if (data == "reged") {
                        $("#regtable-mobile").val("抱歉，该号码已被注册");
                    }  
                } );
            });
    ';
    $this->registerJs($js);
   
    
    
    
    ?>
    
   
        


 

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 
  
    <!-- Include all compiled plugins (below), or include individual files as needed -->
 
    
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>