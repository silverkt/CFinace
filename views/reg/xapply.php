<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Request;
use yii\web\View;
$this->registerCssFile("assets/css/bootstrap.min.css");
 


 
$this->registerJsFile('assets/js/bootstrap.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
 
 


?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      
      
      
      
    <div class="container-fluid">
        <br>
        
        <?php $form = ActiveForm::begin(); ?>
         <?= $form->field($model, 'sex', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['先生' =>'先生','女士'=>'女士'],[
   'prompt' => ['text' => '称呼', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>
        <?= $form->field($model, 'name', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的姓名']) ?>
        
          <?= $form->field($model, 'customer_type', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['授薪' =>'上班族','自雇'=>'企业主'],[
   'prompt' => ['text' => '客户类型', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>   
        
        
                  <?= $form->field($model, 'gongjijin', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['有公积金' =>'有公积金','无公积金'=>'无公积金'],[
   'prompt' => ['text' => '您是否有公积金', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>   
        
        
        <?= $form->field($model, 'pay_cash', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的税前收入(元)'])->label('')  ?>
        
        
            <?= $form->field($model, 'housing_loan', [
             'template' => ' {input} {hint} {error}'
         ])->dropDownList(['有房贷' =>'有房贷','无房贷'=>'无房贷'],[
   'prompt' => ['text' => '您是否有房贷', 'options' => [ 'disabled'=>'true','selected'=>'true']],
]) ?>   
       
        
           <?= $form->field($model, 'housing_loan_per', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的房贷月供（元）'])->label('')  ?>
        
           <?= $form->field($model, 'housing_loan_age', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的已还房贷年限（年）'])->label('')  ?>
        
           <?= $form->field($model, 'enterprise_age', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的企业成立年限(年)'])->label('')  ?>
        
        
           <?= $form->field($model, 'industry', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您所属行业'])->label('')  ?>
        
           <?= $form->field($model, 'enterprise_cash', [
             'template' => ' {input} {hint} {error}'
         ])->input('text',['placeholder' => '在此填入您的企业流水(万元)'])->label('')  ?>
        
        
          
        
        
        
    
        <div class="form-group">
            <?= Html::submitButton('立刻申请', ['class' => 'btn btn-danger', 'style' => 'width:100%;']) ?>
        </div>
    <?php ActiveForm::end(); 

    $js = ' 
    $(".field-regtable-gongjijin").hide();
        $(".field-regtable-tax_list").hide();
            $(".field-regtable-pay_cash").hide();
                $(".field-regtable-housing_loan").hide();
                    $(".field-regtable-housing_loan_per").hide();
                        $(".field-regtable-housing_loan_age").hide();
                            $(".field-regtable-enterprise_age").hide();
                                $(".field-regtable-industry").hide();
                                    $(".field-regtable-enterprise_cash").hide();
                                    
function shoux(){
 $(".field-regtable-gongjijin").show();
        $(".field-regtable-tax_list").show();
            $(".field-regtable-pay_cash").show();
                $(".field-regtable-housing_loan").show();
                    $(".field-regtable-housing_loan_per").hide();
                        $(".field-regtable-housing_loan_age").hide();
                                 $(".field-regtable-enterprise_age").hide();
                                $(".field-regtable-industry").hide();
                                    $(".field-regtable-enterprise_cash").hide();
}


function zig(){
    $(".field-regtable-gongjijin").hide();
        $(".field-regtable-tax_list").hide();
            $(".field-regtable-pay_cash").hide();
                $(".field-regtable-housing_loan").hide();
                    $(".field-regtable-housing_loan_per").hide();
                        $(".field-regtable-housing_loan_age").hide();
                            $(".field-regtable-enterprise_age").show();
                                $(".field-regtable-industry").show();
                                    $(".field-regtable-enterprise_cash").show();
}


function yfd() {
 $(".field-regtable-gongjijin").show();
        $(".field-regtable-tax_list").show();
            $(".field-regtable-pay_cash").show();
                $(".field-regtable-housing_loan").show();
                    $(".field-regtable-housing_loan_per").show();
                        $(".field-regtable-housing_loan_age").show();
                                 $(".field-regtable-enterprise_age").hide();
                                $(".field-regtable-industry").hide();
                                    $(".field-regtable-enterprise_cash").hide();

}


                                    
  $(".field-regtable-customer_type").on("change",function(){
    var type = $("#regtable-customer_type").val() ;
    if(type == "授薪") {
        shoux();
    } else {
        zig();
    }

});


  $(".field-regtable-housing_loan").on("change",function(){
    var type = $("#regtable-housing_loan").val() ;
    if(type == "有房贷") {
        yfd();
    } else {
        showx();
    }
    });



    ';
    $this->registerJs($js);
   
        
?>

 

    </div>

 
    
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>