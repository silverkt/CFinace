<?php
use yii\helpers\Html;
 
 
 
$this->registerCssFile("assets/css/bootstrap.min.css");
 


 
$this->registerJsFile('assets/js/bootstrap.min.js',[ 'depends'=>[\yii\web\JqueryAsset::className()]]);
 
 
$this->title = "提交成功！";
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title> </title>

    <!-- Bootstrap -->
 
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
   
       <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>
      
      
      
      
    <div class="container-fluid">
   
        <div class="row">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10"><br><br>
            <h1 style="text-align: center; color: red; font-size: 150px;"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></h1>
        </div>
        <div class="col-xs-1"> </div>
      </div>
      <div class="row">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10">
          <br>
               <h4>错误的链接，请修改后再次尝试，谢谢！ </h4>
                 <br><br>
               <h6>* 如果您有任何问题，欢迎联系我们的客服：
                   021-62078983 或 <br>登录创富金融官网 <a href="http://www.cf618.com">www.cf618.com</a> 直接给我们留言
               </h6>
              </div>
        </div>
        <div class="col-xs-1"> </div>
      </div>
    <script>        
  
  </script>
    <?php $this->endBody() ?>
  </body>

</html>
<?php $this->endPage() ?>       