<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reg_table".
 *
 * @property integer $id
 * @property integer $loan_sum
 * @property string $property
 * @property string $mobile
 */
class RegTable extends \yii\db\ActiveRecord
{
    public $vercode;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reg_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_sum', 'loan_type', 'mobile', 'vercode', 'ref_id'], 'required', 'on' => 'register'],
            ['mobile', 'unique', 'on' => 'register', 'message' => '此手机号已被注册'],
            ['mobile', 'match',  'on' => 'register', 'pattern' => '/^(13[0-9]|17[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/', 'message' => '手机号码格式不正确'],
            [['loan_sum'], 'integer',  'on' => 'register'],
            ['vercode', 'validateVercode',  'on' => 'register'],
            [['mobile'], 'string', 'max' => 11,  'on' => 'register'],
            
            
            
            [['name', 'sex', 'property', 'backup_property'], 'required', 'on'=> 'dapply'],
            [['property'], 'integer',  'on' => 'dapply'],
            
            [['name', 'sex'], 'required', 'on'=> 'xapply'],
            [['customer_type', 'gongjijin','tax_list','pay_cash','housing_loan','housing_loan_per', 'housing_loan_age','enterprise_age','industry', 'enterprise_cash'], 'safe', 'on'=> 'xapply'],
             [['customer_type', 'gongjijin','pay_cash','housing_loan','housing_loan_per', 'housing_loan_age','enterprise_age','industry', 'enterprise_cash'], 'default',  'value'=> 'N/A', 'on'=> 'xapply'],
            
            
        ];
    }
    
    
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'loan_sum' => '贷款额度（万）',
            'loan_type' => '贷款类型',
            'mobile' => '手机号码',
            'vercode' => '验证码',
            'name' => '姓名',
            'sex' => '性别',
            'property' => '房产总价（万）',
            'backup_property' => '有无备用房',
            'customer_type' => '客户类型',
            'gongjijin' => '公积金',
            'tax_list' => '税单',
            'pay_cash' => '税前收入（元）',
            'housing_loan' => '房贷',
            'housing_loan_per' => '月供（元）',
            'housing_loan_age' => '已还房贷年限（年）',
            'enterprise_age' => '公司成立多久（年）',
            'industry' => '行业',
            'enterprise_cash' => '公司流水'
        ];
    }
    
     /**
     * Validates the vercode.
     * This method serves as the inline validation for vercode.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateVercode($attribute, $params)
    {
        //检查session是否打开
        if (!Yii::$app->session->isActive) {
            Yii::$app->session->open();
        }
        //取出session中储存的验证码和短信发送时间
        $sms_code = Yii::$app->session->get('sms_code');
        $sms_time = Yii::$app->session->get('sms_time');
         if ($sms_code != $this->vercode) {
            $vercode_e = '验证码错误';
            if (!$this->hasErrors()) {
                $this->addError($attribute, $vercode_e);
            }
        }
        if (time() > ($sms_time+180)) {
            $vercode_e = '验证码已过期';
            if (!$this->hasErrors()) {
                $this->addError($attribute, $vercode_e);
            }
        }
       
    }



    public static function sendVerCode()
    {    
        $mobile = Yii::$app->request->post('mobile');    
        $code = mt_rand(100000,999999);  
        //$code = 1234;
        $smsTemplate =  "尊敬的客户您好！您的验证码是：".$code."。请不要把验证码泄露给其他人。";
        if (self::findOne(['mobile'=>$mobile])) {
            echo 'reged';
            exit;
        }
          if (self::sendtoGate($mobile,$smsTemplate)===true) {
            //if (true) {    
            //检查session是否打开
            if (!Yii::$app->session->isActive) {
                Yii::$app->session->open();
            }
            //验证码和短信发送时间存储session
            Yii::$app->session->set('sms_code',$code);
            Yii::$app->session->set('sms_time',time());
            echo 'codesent';
            return;
        } else {
            echo  'Failed';
            return;
        }    
    }
    
    
    public static function sendInfo($mobile,$sms)
    {    
         $smsTemplate = "新订单 ".$sms."。";
         self::sendtoGate($mobile, $smsTemplate);
         return true;
    }

    public static function sendtoGate($mobile,$sms)
    {
        $url = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";
        $curlPost = "account=C01911893&password=90fbcb21108d173acad4190442299d08&mobile=".$mobile."&content=".rawurlencode($sms);      
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
        $return_str = curl_exec($curl);
        curl_close($curl);
        return true;
    }    
    
}
