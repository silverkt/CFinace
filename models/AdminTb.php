<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_tb".
 *
 * @property integer $id
 * @property string $passwd
 * @property string $name
 * @property string $mobile
 */
class AdminTb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_tb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passwd'], 'string', 'max' => 32],
            [['name', 'mobile'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passwd' => '密码',
            'name' => '姓名',
            'mobile' => '手机',
        ];
    }
}
