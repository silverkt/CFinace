<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "regcode".
 *
 * @property integer $id
 * @property string $code
 * @property integer $expired
 * @property string $date
 */
class Regcode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regcode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'expired', 'date'], 'required'],
            [['expired'], 'integer'],
            [['code', 'date'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'expired' => 'Expired',
            'date' => 'Date',
        ];
    }
}
